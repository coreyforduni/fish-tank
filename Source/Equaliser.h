/*
  ==============================================================================

    Equaliser.h
    Created: 30 Oct 2018 2:39:56pm
    Author:  Corey Ford

  ==============================================================================
*/
/*
 ==============================================================================
 Partly derived through watching this video:
 - https://www.youtube.com/watch?v=2O3nm0Nvbi4
 ==============================================================================
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "FFT.h"

//==============================================================================

namespace FishTank
{
    /**
     *  Visual representation of an FFT equaliser.
     */
    class Equaliser    : public Component
    {
    public:
        /**
         *  Constructor.
         *  @param reference to the FFT the Equaliser will draw from.
         */
        Equaliser (FFT& fftRhs);
        
        /** Destructor */
        ~Equaliser();
        
        /**
         *  Paint callback.
         *  @param the graphics context for this object.
         */
        void paint (Graphics&) override;
        
        /** Resized Callback. Sets bounds of child components. */
        void resized() override;
        
        /** Draw's FFT information to image. */
        void drawEqualiser();
        
    private:
        /** Private constructor. FFT must be specified.*/
        Equaliser();
        
        Image image; ///< the image our FFT is drawn to.
        FFT& fft; ///< reference to our FFT object.
        
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Equaliser)
    };

} // namespace FishTank

