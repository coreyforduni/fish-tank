/*
  ==============================================================================

    Equaliser.cpp
    Created: 30 Oct 2018 2:39:56pm
    Author:  Corey Ford

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "Equaliser.h"

//==============================================================================
namespace FishTank
{
    Equaliser::Equaliser (FFT& fftRhs) : image (Image::RGB, 512, 512, true), fft (fftRhs)
    {
        setOpaque (false);
    }
    
    Equaliser::~Equaliser()
    {
    }
    
    void Equaliser::paint (Graphics& g)
    {
        g.setOpacity (0.75f);
        g.drawImage (image, getLocalBounds().toFloat());
    }
    
    void Equaliser::drawEqualiser()
    {
        // Ensure the image is blank before drawing
        image.clear (image.getBounds());
        
        // Get the actual data array size as the other half of the array is
        // only for calculations.
        auto actualDataSize = fft.getSize() / 2;
        
        int barWidth = image.getWidth() / actualDataSize;
        
        // for every step in the fft
        for (int i = 0; i < actualDataSize; ++i)
        {
            // find the min and max
            float fftMax = FloatVectorOperations::findMaximum (fft.getData(), actualDataSize);
            float fftMin = FloatVectorOperations::findMinimum (fft.getData(), actualDataSize);

            if (fftMax != 0) // if audio if playing...
            {
                // calculate the bar height
                float barHeight = jmap(fft.getData()[i], fftMin, fftMax, 0.0f, (float)image.getHeight());
                
                for (int k = 0; k < (int)barHeight; ++k)
                {
                    for (int j = 0; j < barWidth; j++)
                        image.setPixelAt(i + j, (image.getHeight() - k), Colours::blue);
                }
                
            }
        }
    }
    
    void Equaliser::resized() { }

}
