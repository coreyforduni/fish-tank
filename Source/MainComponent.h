/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioFilePlayer.h"
#include "Spectrogram.h"
#include "Equaliser.h"
#include "FFT.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/

namespace FishTank
{
    /**
     *  Main window component.
     */
    class MainComponent   : public AudioAppComponent,
                            public Timer
    {
    public:
        //==============================================================================
        /** Constructor. */
        MainComponent();
        
        /** Destructor. */
        ~MainComponent();
        
        //==============================================================================
        /**
         *  Actions to occour before the audio block callback starts.
         *  @param the number of samples per block.
         *  @param the sample rate to be used.
         */
        void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
        
        /**
         *  Audio callback for each block.
         *  @param The audio buffer containing the channel data.
         */
        void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
        
        /**
         *  Wrapper for releasing the transport source.
         *  Called when audio block is finished.
         */
        void releaseResources() override;
        
        //==============================================================================
        /**
         *  Paint callback.
         *  @param the graphics context for this object.
         */
        void paint (Graphics& g) override;
        
        /** Resized Callback. Sets bounds of child components. */
        void resized() override;
        
        //==============================================================================
        /** Callback for the timer. */
        void timerCallback() override;

    private:
        //==============================================================================
        // Your private member variables go here...
        AudioFilePlayer audioFilePlayer; ///< Handles loading and playback of audio files.
        Equaliser equaliser; ///< Equaliser.
        Spectrogram spectrogram; ///< Spectrogram
        FFT fft; ///< Our FFT object.
        
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    };

} // namespace FishTank
