/*
  ==============================================================================

    Spectrogram.cpp
    Created: 23 Oct 2018 2:32:56pm
    Author:  Corey Ford

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "Spectrogram.h"

//==============================================================================
namespace FishTank
{
    Spectrogram::Spectrogram (FFT& fftRhs) : image (Image::RGB, 512, 512, true), fft (fftRhs)
    {
        setOpaque (false);
    }
    
    Spectrogram::~Spectrogram()
    {
    }
    
    void Spectrogram::paint (Graphics& g)
    {
        g.setOpacity (0.3f);
        g.drawImage (image, getLocalBounds().toFloat());
    }
    
    void Spectrogram::drawNextLineOfSpectrogram()
    {
        // move left one pixel away from the original height
        auto rightHandEdge = image.getWidth() - 1;
        auto imageHeight = image.getHeight();
        image.moveImageSection (0, 0, 1, 0, rightHandEdge, imageHeight);
        
        // find the max level that has happened to the fft, so that we can scale our spectogram accordingly
        auto maxLevel = FloatVectorOperations::findMinAndMax (fft.getData(), fft.getSize() / 2);
        
        // for each pixel calculate the level
        for (auto pixel = 1; pixel < imageHeight; ++pixel)
        {
            // skew to a logarithmic scale
            auto skewedProportionY = 1.0f - std::exp (std::log (pixel / (float) imageHeight) * 0.2f);
            
            // use this line if not logarithmic //TODO: linear setting
//             auto skewedProportionY = 1.0f - (pixel / (float) imageHeight);
            
            // limit the amplitude range between 0 - 1.0
            auto fftDataIndex = jlimit (0, fft.getSize() / 2, (int) (skewedProportionY * fft.getSize() / 2));
            
            //calculate the level
            auto level = jmap (fft.getData()[fftDataIndex], 0.0f, jmax (maxLevel.getEnd(), 1e-5f), 0.0f, 1.0f);
            
            auto rgbLevel = level * 255.0;
            
            //finally.. set the pixel colour
            image.setPixelAt (rightHandEdge, pixel, Colour::fromRGBA (0, rgbLevel, 255-rgbLevel, rgbLevel));
        }
    }
    
    void Spectrogram::resized() { }

}
