/*
  ==============================================================================

    FFT.h
    Created: 23 Oct 2018 2:02:01pm
    Author:  Corey Ford

  ==============================================================================
*/
/*
 ==============================================================================
 Derived through completion of this tutorial:
 - https://docs.juce.com/master/tutorial_simple_fft.html
 ==============================================================================
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

namespace FishTank
{
    /**
     *  Wrapper for running FFT calculations on a FIFO buffer.
     */
    class FFT 
    {
    public:
        
        /** Constructor. */
        FFT();
        
        /** Destructor. */
        ~FFT(); 

        /**
         *  Audio callback for each block.
         *  @param The audio buffer containing the channel data.
         */
        void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill);
        
        /**
         *  Performs the FFT calculation.
         */
        void processFFTBlock();
        
        /**
         *  Getter for our data array (containing FFT results).
         *  @return a pointer to the front of our data (result) array.
         */
        const float* getData() const;
        
        /**
         *  Getter for the size of our data (result) array.
         *  @return the size of our data (result) array.
         */
        const int getSize() const;
        
        /**
         *  Getter to say our block is ready.
         *  @return true if our window of samples is full.
         */
        const bool& isNextBlockReady() const;
        
        /**
         *  Setter to say if our next block is ready.
         *  @param true if our window of samples is full.
         */
        void setNextBlockReady (bool isReady); 
        
        /** Enum holding FFT constants. */
        enum
        {
            fftOrder = 10, // 2 to the power of.. number of points the FFT will operate
            fftSize = 1 << fftOrder // to the power of
        };

    private:
        
        /**
         *  Pushes the sample onto our FIFO buffer.
         *  @param sample to be pushed.
         */
        void pushNextSampleIntoFifo (float sample);
        
        dsp::FFT forwardFFT; ///< object to perform the FFT on
        float fifo [fftSize]; ///< buffer containing our audio examples
        float fftData [2 * fftSize]; ///< will contain the results of our fft calculations
        int fifoIndex = 0; ///< keeps count of the number of samples in the fifo
        bool nextFFTBlockReady = false; ///< tells us when the next FFT is ready to be rendered

    };
    
} // namespace FishTank
