/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

namespace FishTank
{
    //==============================================================================
    MainComponent::MainComponent() : equaliser (fft), spectrogram (fft)
    {
        setAudioChannels (0, 2);
        startTimerHz (60);
        
        addAndMakeVisible (audioFilePlayer);
        addAndMakeVisible (equaliser);
        addAndMakeVisible (spectrogram);
        
        setSize (700, 500);
    }
    
    MainComponent::~MainComponent()
    {
        // This shuts down the audio device and clears the audio source.
        shutdownAudio();
    }
    
    //==============================================================================
    void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
    {
        audioFilePlayer.prepareToPlay (samplesPerBlockExpected, sampleRate);
    }
    
    void MainComponent::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
    {
        audioFilePlayer.getNextAudioBlock (bufferToFill);
        fft.getNextAudioBlock (bufferToFill);
    }
    
    void MainComponent::releaseResources()
    {
        audioFilePlayer.releaseResources();
    }
    
    //==============================================================================
    void MainComponent::paint (Graphics& g)
    {
        g.fillAll (Colours::black);
    }
    
    void MainComponent::resized()
    {
        Rectangle<int> top, bottom;
        bottom = getLocalBounds().removeFromBottom (getHeight() * 0.75);
        top = getLocalBounds().removeFromTop (getHeight() * 0.25);
        
        audioFilePlayer.setBounds (top);
        equaliser.setBounds (bottom);
        spectrogram.setBounds (bottom);
    }
    
    //==============================================================================
    void MainComponent::timerCallback()
    {
        if (fft.isNextBlockReady())
        {
            // Do the fft onto the data array
            fft.processFFTBlock();

            spectrogram.drawNextLineOfSpectrogram();
            spectrogram.repaint();

            equaliser.drawEqualiser();
            equaliser.repaint();
            
            fft.setNextBlockReady (false);
        }
    }

}
