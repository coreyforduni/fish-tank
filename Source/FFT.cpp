/*
  ==============================================================================

    FFT.cpp
    Created: 23 Oct 2018 2:02:01pm
    Author:  Corey Ford

  ==============================================================================
*/

#include "FFT.h"

namespace FishTank
{
    FFT::FFT() : forwardFFT (fftOrder)
    {
        fifoIndex = 0;
        nextFFTBlockReady = false;
    }
    
    FFT::~FFT()
    {
        
    }
    
    void FFT::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
    {
        // ...simply push all the samples contained in our current audio buffer block
        // to the fifo to be processed at a later time
        if (bufferToFill.buffer->getNumChannels() > 0)
        {
            auto* channelData = bufferToFill.buffer->getReadPointer (0, bufferToFill.startSample);
            for (auto i = 0; i < bufferToFill.numSamples; ++i)
                pushNextSampleIntoFifo (channelData[i]);
        }
    }
    
    void FFT::pushNextSampleIntoFifo (float sample)
    {
        // if the fifo contains enough data...
        if (fifoIndex == fftSize)
        {
            // set a flag to say that the next line should now be rendered..
            if (! nextFFTBlockReady)
            {
                zeromem (fftData, sizeof (fftData));
                memcpy (fftData, fifo, sizeof (fifo));
                nextFFTBlockReady = true;
            }
            fifoIndex = 0;
        }
        
        fifo[fifoIndex++] = sample;  // else push the sample into the fifo
    }
    
    void FFT::processFFTBlock()
    {
        forwardFFT.performFrequencyOnlyForwardTransform (fftData);
    }
    
    const float* FFT::getData() const
    {
        return fftData;
    }
    
    const int FFT::getSize() const
    {
        return fftSize;
    }
    
    const bool& FFT::isNextBlockReady() const
    {
        return nextFFTBlockReady;
    }
    
    void FFT::setNextBlockReady (bool isReady)
    {
        nextFFTBlockReady = isReady;
    }

} // namespace FishTank
