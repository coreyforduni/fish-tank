/*
  ==============================================================================

    AudioFilePlayer.cpp
    Created: 29 Oct 2018 9:25:35pm
    Author:  Corey Ford

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioFilePlayer.h"

namespace FishTank
{
    AudioFilePlayer::AudioFilePlayer()
    {
        // when the program first loads audio is stopped.
        state = Stopped;
        
        // setup open button
        addAndMakeVisible (&openButton);
        openButton.setButtonText ("Open...");
        openButton.onClick = [this] { openButtonClicked(); };
        
        // setup play button
        addAndMakeVisible (&playButton);
        playButton.setButtonText ("Play");
        playButton.onClick = [this] { playButtonClicked(); };
        playButton.setColour (TextButton::buttonColourId, Colours::green);
        playButton.setEnabled (false);
        
        // setup stop button
        addAndMakeVisible (&stopButton);
        stopButton.setButtonText ("Stop");
        stopButton.onClick = [this] { stopButtonClicked(); };
        stopButton.setColour (TextButton::buttonColourId, Colours::red);
        stopButton.setEnabled (false);
        
        formatManager.registerBasicFormats();       // say yes.. we may have wav's and similar (part of module)
        transportSource.addChangeListener (this);   // we want to respond to changes in its state
    }
    
    AudioFilePlayer::~AudioFilePlayer(){}
    
    void AudioFilePlayer::paint (Graphics& g)
    {
    }
    
    void AudioFilePlayer::resized()
    {
        auto all = getLocalBounds();
        all.setHeight (all.getHeight() * 0.3333333);
        
        openButton.setBounds (all);
        playButton.setBounds (all.translated (0, all.getHeight()));
        stopButton.setBounds (all.translated (0, all.getHeight() * 2));
    }
    
    //==========================================================================

    void AudioFilePlayer::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
    {
        transportSource.prepareToPlay (samplesPerBlockExpected, sampleRate);
    }
    
    void AudioFilePlayer::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
    {
        if (readerSource.get() == nullptr)
        {
            bufferToFill.clearActiveBufferRegion();
            return;
        }
        
        transportSource.getNextAudioBlock (bufferToFill); // get samples from transport source
    }
    
    void AudioFilePlayer::releaseResources()
    {
        transportSource.releaseResources();
    }
    
    //==========================================================================
    
    void AudioFilePlayer::changeListenerCallback (ChangeBroadcaster* source)
    {
        if (source == &transportSource)
        {
            if (transportSource.isPlaying())
            {
                changeState (Playing); // updating enum
            }
            else if ((state == Stopping) || (state == Playing))
            {
                changeState (Stopped);
            }
            else if (state == Pausing)
            {
                changeState (Paused); // updating enum
            }
        }
    }
    
    //==========================================================================
    
    void AudioFilePlayer::changeState (TransportState newState)
    {
        if (state != newState)
        {
            state = newState;
            
            switch (state)
            {
                case Stopped: // so we want to set the buttons, reset to start of file
                    stopButton.setEnabled (false);
                    playButton.setEnabled (true);
                    openButton.setEnabled (true);
                    transportSource.setPosition (0.0); // reset to start of file
                    break;
                    
                case Starting:
                    playButton.setEnabled (false);
                    transportSource.start(); // lets start playing
                    break;
                    
                case Playing: // update buttons
                    openButton.setEnabled (false);
                    stopButton.setEnabled (true);
                    break;
                    
                case Stopping:
                    transportSource.stop(); // STOP!
                    break;
                    
                case Pausing:
                    transportSource.stop();
                    break;
                    
                case Paused:
                    playButton.setButtonText ("Resume");
                    stopButton.setButtonText ("Return to zero");
                    break;
            }
        }
    }
    
    void AudioFilePlayer::openButtonClicked()
    {
        FileChooser chooser ("Select a Wave file to play...", File(), "*.wav");
        
        if (chooser.browseForFileToOpen())
        {
            auto file = chooser.getResult();
            auto* reader = formatManager.createReaderFor (file); // try to create a reader for this type of file
            
            if (reader != nullptr)
            {
                std::unique_ptr<AudioFormatReaderSource> newSource (new AudioFormatReaderSource (reader, true));
                transportSource.setSource (newSource.get(), 0, nullptr, reader->sampleRate);
                playButton.setEnabled (true);
                readerSource.reset (newSource.release());
            }
        }
    }
    
    void AudioFilePlayer::playButtonClicked()
    {
        if( (state == Stopped) || (state == Paused) )
            changeState (Starting);
        else if (state == Playing)
            changeState (Pausing);
    }
    
    void AudioFilePlayer::stopButtonClicked()
    {
        if (state == Paused)
        {
            changeState (Stopped);
        }
        else
        {
            changeState (Stopping);
        }
    }
    
} // namespace FishTank
