# Fish-Tank

**For UWE's ADC Competition 2018!**

I admit that Fish-Tank dosen't make any sense. I had an initial idea to make fish follow parts of the 
spectrogram, however got distracted with other things. 

Hope you still enjoy the vaugley fish tank coloured visualisations! 

Also feel free to look at the commit history [here](https://bitbucket.org/coreyforduni/fish-tank/src/master/). 
___

## My Contact Info

* **Name:** Corey Ford
* **Email:** corey2.ford@live.uwe.ac.uk
* **Phone Number:** 07834 244736

___

## Getting Started

The application will load any wav file and visualise both an equaliser and sonogram at the same time. 

**Prerequisites**

* You will need to get JUCE from [here](https://shop.juce.com/get-juce) if you don't have the modules already.
* Then simply plug them in as global paths. 
___

## Authors

* Corey Ford

___

## Acknowledgments

* Thanks to... 
