/*
  ==============================================================================

    AudioFilePlayer.h
    Created: 29 Oct 2018 9:25:35pm
    Author:  Corey Ford

  ==============================================================================
*/
/*
 ==============================================================================
 Derived through completion of this tutorial:
 - https://docs.juce.com/master/tutorial_playing_sound_files.html
 ==============================================================================
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
namespace FishTank
{
    /**
     *  Class handling audio playback.
     */
    class AudioFilePlayer    : public Component, public ChangeListener
    {
    public:
        /** Constructor. */
        AudioFilePlayer();
        
        /** Destructor. */
        ~AudioFilePlayer();
        
        /**
         *  Paint callback.
         *  @param the graphics context for this object.
         */
        void paint (Graphics&) override;
        
        /** Resized Callback. Sets bounds of child components. */
        void resized() override;

        //======================================================================
        /**
         *  Actions to occour before the audio block callback starts.
         *  @param the number of samples per block.
         *  @param the sample rate to be used.
         */
        void prepareToPlay (int samplesPerBlockExpected, double sampleRate);
        
        /**
         *  Audio callback for each block.
         *  @param The audio buffer containing the channel data.
         */
        void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill);
        
        /** Wrapper for releasing the transport source. */
        void releaseResources();
        
        //======================================================================
        
        /**
         *  Callback for when the transport source changes state.
         *  @param the broadcaster that triggered the callback.
         */
        void changeListenerCallback (ChangeBroadcaster* source) override;

    private:
        /** Enumeration of different transport states. */
        enum TransportState
        {
            Stopped,
            Starting,
            Playing,
            Stopping,
            Pausing,
            Paused
        };
        
        /**
         *  Setter for buttons and the transport source based on the new state.
         *  @param the state we are changing to.
         */
        void changeState (TransportState newState);
        
        /** File actions to be performed when the open button is clicked. */
        void openButtonClicked();

        /** Triggers playback when the play button is clicked. */
        void playButtonClicked();
        
        /** Stops playback and resets the playhead when the stop button is clicked. */
        void stopButtonClicked();
        
        //==========================================================================
        TextButton openButton;
        TextButton playButton;
        TextButton stopButton;
        
        AudioFormatManager formatManager; ///< handles if we add a WAV or OGG or whatever
        std::unique_ptr<AudioFormatReaderSource> readerSource; ///< reads from the format manager (e.g. makes array of floats)
        AudioTransportSource transportSource; ///< handles playing, stopping etc inheriting from AudioScource (handles taking the reader source and senind it to the audio
        TransportState state; ///< enum (see above)
        
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioFilePlayer)
    };
} // namespace FishTank
