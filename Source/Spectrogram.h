/*
  ==============================================================================

    Spectrogram.h
    Created: 23 Oct 2018 2:32:56pm
    Author:  Corey Ford

  ==============================================================================
*/
/*
 ==============================================================================
 Derived through completion of this tutorial:
 - https://docs.juce.com/master/tutorial_simple_fft.html
 ==============================================================================
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "FFT.h"

//==============================================================================

namespace FishTank
{
    /**
     *  A logarithmic spectrogram object.
     */
    class Spectrogram    : public Component
    {
    public:
        /**
         *  Constructor.
         *  @param reference to the FFT the spectrogram will draw from.
         */
        Spectrogram (FFT& fftRhs);
        
        /** Destructor */
        ~Spectrogram();
        
        /**
         *  Paint callback.
         *  @param the graphics context for this object.
         */
        void paint (Graphics&) override;
        
        /** Resized Callback. Sets bounds of child components. */
        void resized() override;
        
        /** Draw's FFT information to image. */
        void drawNextLineOfSpectrogram();
        
    private:
        /** Private constructor. FFT must be specified.*/
        Spectrogram();
        
        Image image; ///< the image our FFT is drawn to.
        FFT& fft; ///< reference to our FFT object.
        
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Spectrogram)
    };

} // namespace FishTank

